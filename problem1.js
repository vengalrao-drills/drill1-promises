function racePromise1(time) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("racePromise1 got resolved");
    }, time * 1000); // time calculated is in milliseconds. so multiplied the number to 1000
  }); 
}

function racePromise2(time) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("racePromise2 got resolved");
    }, time * 1000); 
  });
}

function racePromises() {
  // Math.random() gives random decimal number that is a type of ( 0.2344535-----)
  // it is always a zero

  // Math.random()*3 -> it ranges values ranges from ( 0-3 max ) the put put will be in decimals
  // if i use Math.ceil() then  it converts to the integer.

  let timeRace1 = Math.ceil(Math.random() * 3);
  let timeRace2 = Math.ceil(Math.random() * 3);
  console.log(timeRace1, timeRace2);
  // Promise.race() return the first Promise value. 
  // whether its reject or resolve. when ever it received the promise. then it returns info.
  Promise.race([racePromise1(timeRace1), racePromise2(timeRace2)])  
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.log(err);
    });
}

racePromises();
