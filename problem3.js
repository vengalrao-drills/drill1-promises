function promise1() {
  return new Promise((resolve, reject) => {
    let value = "Promise1";
    resolve(value);    // returning the value  
  });
}

function promise2(data) {
  return new Promise((resolve, reject) => {
    let value = "Promise2";
    resolve(`${data} ${value}`); // combined the recieved data with the local variable.
  });
}

function promise3(data) {
  return new Promise((resolve, reject) => {
    let value = "Promise3";
    resolve(`${data} ${value}`);  // combined the recieved data with the local variable.
  });
}

function dynamicChain(array) {
  let resolved = Promise.resolve()
  for (let index = 0; index < array.length; index++) {
    resolved = resolved.then((data) => array[index](data))
  }
  return resolved
}


let array = [promise1, promise2, promise3]
let answer = dynamicChain(array)
answer.then((data) => {
  console.log(data)
}).catch((error) => {
  console.log(error)
})

// function dynamicChain() {
//   promise1()
//     .then((data) => {
//       console.log("promise1 function returned output - - - - -", data);  // function called and we recieved the data. now the data nned to be sent to the next promise.
//       return promise2(data); // data sended.
//       // if we wont use return function. then we have have to write the  .then() inside the function. 
//       // here we are returning the function.
//     })
//     .catch((error) => {
//       console.log(error); // if error it prints. the error.
//     })
//     .then((data) => {
//       console.log("promise2 function returned output - - - - -", data);
//       return promise3(data); // same again sending the  recieved data.
//     })
//     .catch((error) => {
//       console.log(error);
//     })
//     .then((data) => {
//       console.log(
//         "this is the final, promise3 function returned output - - - - -",
//         data
//       );
//     })
//     .catch((error) => {
//       console.log(error);
//     });
// }
// dynamicChain();
