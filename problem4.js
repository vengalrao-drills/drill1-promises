function promise1() {
    return new Promise((resolve, reject) => {
        resolve("Promise1 resolved");
    });
}

function promise2() {
    return new Promise((resolve, reject) => {
        resolve("Promise2 resolved");
    });
}

function promise3() {
    return new Promise((resolve, reject) => {
        resolve("Promise3 resolved");
    });
}

function promise4() {
    return new Promise((resolve, reject) => {
        resolve("Promise4 resolved");
    });
}

function promise5() {
    return new Promise((resolve, reject) => {
        resolve("Promise5 resolved");
    });
}


//created 5 five promises. 

// used async becoz, i am using await  inside function.
async function perfromIt(myPromises) {
    let allAnswers = [];
    for (let index = 0; index < myPromises.length; index++) {
        let checkPromise = myPromises[index];
        //using await because we response.
        // now the the function will pause and wait for the response whether its reject or resolve.
        await checkPromise.then((data) => {
            allAnswers.push(data)
            // console.log(data)
        }).catch((error) => {
            allAnswers.push(error)
            // console.log(error)
        })
    }
    // console.log('allAnswers ', allAnswers)
    return allAnswers
}


async function parallelLimit() {
    let allPromises = [promise1(), promise2(), promise3(), promise4(), promise5()] // all promises
    let limit = 2;
    let sendIt = []
    let finalAanswers = []
    for (let index = 0; index < allPromises.length; index++) {
        if (sendIt.length < limit) {
            sendIt.push(allPromises[index])
        } else {
            try{
                let returnedAnswer = await perfromIt(sendIt) // the function will wait for the response. then proceeds. else error will come.
            // console.log('returnedAnswer ', returnedAnswer)
            // finalAanswers = [...finalAanswers , returnedAnswer]  // here destructuring
            finalAanswers = finalAanswers.concat(returnedAnswer) // it directly concats the array
            sendIt = []
            sendIt.push(allPromises[index])

            }
            catch(error){
                console.log(error)
            }
            
        }
    }
    // console.log(finalAanswers.flat()) // if we destructure and add. array will be in mutidimensional array. as it has array received. 
    console.log(finalAanswers)
}

parallelLimit()
 
