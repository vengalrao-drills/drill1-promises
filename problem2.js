function promise1() {
  return new Promise((resolve, reject) => {
    // resolve("function Name :- Promise1 Resolved");
    reject("function Name :- Promise1 Reject");
  });
}

function promise2() {
  return new Promise((resolve, reject) => {
    resolve("function Name :- Promise2 Resolved");
  });
}

function composePromises() {
    // Promise.all([])  ----> we need to send the promises function in side the array. 
    // if all the promises got resolved. then, we will receive the status of the given promise. (in the form of Array)
    // if ERROR in the promise. then we only GET THAT PROMISE GOT REJECTED.
  return Promise.all([promise2(), promise1()]);
}

// console.log(composePromises())

// same as usual like Promise returns a object. Promise.all() also return the object.
composePromises()
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.log(error);
  });


 